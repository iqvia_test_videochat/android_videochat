package com.tokbox.android.tutorials.simple_multiparty;

import android.util.Log;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

/**
 * FileUtils.
 *
 * @author ccollins
 *
 */
final class LogUtils {

    // Object for intrinsic lock (per docs 0 length array "lighter" than a normal Object
    public static final Object[] DATA_LOCK = new Object[0];

    private LogUtils() {
    }

    /**
     * Append String to end of File.
     *
     * @param appendContents
     * @return
     */
    public static boolean appendLogToFile(final String appendContents) {
        boolean result = false;
        try {
            synchronized (DATA_LOCK) {
                File logFile = new File("sdcard/multiparty_log.txt");
                if (!logFile.exists())
                {
                    try
                    {
                        logFile.createNewFile();
                    }
                    catch (IOException e)
                    {
                        e.printStackTrace();
                    }
                }
                if (logFile != null && logFile.canWrite()) {
                    BufferedWriter out = new BufferedWriter(new FileWriter(logFile, true), 1024);
                    Date currentTime = Calendar.getInstance().getTime();
                    out.append(currentTime.toString() + ": " + appendContents);
                    out.newLine();
                    out.newLine();
                    out.append("============================================================");
                    out.newLine();
                    out.newLine();
                    out.close();
                    result = true;
                }
            }
        } catch (IOException e) {
            Log.e("SAVING_LOG_FILE", "Error writing string data to file " + e.getMessage(), e);
        }
        return result;
    }
}